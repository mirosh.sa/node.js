const noteRoutes = require('./note_routes');
const siteRoutes = require('./site_routes');
module.exports = function (app, db) {
    noteRoutes(app, db);
    siteRoutes(app, db);
};