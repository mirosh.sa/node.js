module.exports = function (app, db) {
    app.set('views', './views');
    app.set('view engine', 'pug');

    app.get('/', (req, res) => {
        res.render('index', { title: 'Hey', message: 'Hello there!'});
    });
};