const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const db = require('./config/db');
const sassMiddleware = require('node-sass-middleware');
const app = express();
const port = 8000;

app.use(sassMiddleware({
    src: __dirname + '/scss',
    response: false,
    dest: __dirname + '/public', // not able to set another directory
    outputStyle: 'extended'
}));




app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));

MongoClient.connect(db.url, (err, database) => {
    if (err) return console.log(err)
    require('./app/routes')(app, database);
    app.listen(port, () => {
        console.log('We are live on ' + port);
    });
});